import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import Title from '../Title';

test('Title组件渲染内容为Hello World', () => {
  const { getByTestId } = render(<Title />);
  expect(getByTestId('title')).toHaveTextContent('Hello World');
});
